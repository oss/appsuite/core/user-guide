# User Guide aka the Online Help

This repository contains the App Suite Online Help in all supported languages.

Some details about the content of this repository:
- The file format is html
- The files are generated from the DocBook XML sources files. These source files can be found here:\
https://gitlab.open-xchange.com/documentation/appsuite

Details about creating the Online Help:
- The process for building the Online Help is described on this User Documentation Wiki page:\
https://gitlab.open-xchange.com/appsuite/web-apps/user-guide/-/wikis/0-Process-~-Docu-workflow-and-tools
- The startpage of the User Documentation Wiki can be found here:\
https://gitlab.open-xchange.com/appsuite/web-apps/user-guide/-/wikis/home

Infos about legal and normative requirements for user documentation can be found here:
- https://gitlab.open-xchange.com/appsuite/web-apps/user-guide/-/wikis/6-Legal-and-normative-requirements/
